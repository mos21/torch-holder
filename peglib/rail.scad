// NOSTL

module _rail(height, chamfer = 0, back_wall_gap, side_wall_gap) {

    translate([0, wall_thickness - back_wall_gap, 0]) {

        // back
        chamfered_box([base_size - (side_wall_gap * 2), wall_thickness - back_wall_gap, base_size * height], align = [0, -1, 1], chamfer = chamfer);

        // right side
        translate([(base_size / 2) - side_wall_gap , 0, 0])
            chamfered_box([wall_thickness - side_wall_gap, wall_thickness * 3, base_size * height], align = [-1, -1, 1], chamfer = chamfer);
            
        // right lip
        translate([(base_size / 2) - side_wall_gap, wall_thickness * -2, 0])
            chamfered_box([(wall_thickness * 2) - side_wall_gap, wall_thickness, base_size * height], align = [-1, -1, 1], chamfer = chamfer);

        // left side
        translate([-((base_size / 2) - side_wall_gap), 0, 0])
            chamfered_box([wall_thickness - side_wall_gap, wall_thickness * 3, base_size * height], align = [1, -1, 1], chamfer = chamfer);

        // left lip
        translate([-((base_size / 2) - side_wall_gap) , wall_thickness * -2, 0])
            chamfered_box([(wall_thickness * 2) - side_wall_gap, wall_thickness, base_size * height], align = [1, -1, 1], chamfer = chamfer);

        // top
        translate([0, 0, ((base_size * height) - wall_thickness) ])
            chamfered_box([base_size - (side_wall_gap * 2), wall_thickness * 3, wall_thickness], align = [0, -1, 1], chamfer = chamfer);    
            
        // top lip
        translate([0, -wall_thickness * 3, (base_size * height) ])
            chamfered_box([base_size - (side_wall_gap * 2), wall_thickness, wall_thickness * 2], align = [0, 1, -1], chamfer = chamfer);    

    }
}



module rail(height, chamfer = 0, back_wall_gap = default_back_wall_gap, side_wall_gap = 0, copies = 1) {
    translate([(base_size * (copies - 1)) * -0.5, 0, 0]) {
        for(copy = [1:copies]) {
            translate([base_size * (copy - 1), 0, 0]) {
                _rail(height, chamfer, back_wall_gap = back_wall_gap, side_wall_gap = side_wall_gap);
                if(copy > 1) {
                    translate([-base_size / 2, wall_thickness - back_wall_gap, 0])
                    color("cyan") 
                            chamfered_box([wall_thickness * 2, wall_thickness * 3, base_size * height], align = [0, -1, 1], chamfer = chamfer);

                }
            }
        }
    }
}


/*

color("blue") rail(height = 2, chamfer = 1);

translate([0, base_size, 0]) {
    color("red") rail(height = 2, chamfer = 1, copies = 2, side_wall_gap = 0.25);
    translate([0, base_size, 0]) {
        color("green") rail(height = 2, chamfer = 1, copies = 3);
    }
}

*/

