// NOSTL

include <peglib/defaults.scad>;
include <moslib/libchamfer.scad>;
include <peglib/rail.scad>;


wideness = 2;

$fn = 200;

tube_outer_gap = 1;
cap_back_wall_gap = 0.2;
chamfer = (wall_thickness / 2) / 2;

id = 28;
od = id + (wall_thickness * 2);

