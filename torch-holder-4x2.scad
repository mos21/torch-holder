tallness = 4; // magic:tallness:[2,3,4]
caps = 2; // magic:caps:[1,2]
cutout = false; // magic:cutout:[true,false]

include <torch-defaults.scad>;

height = base_size * tallness;

// move edge of tube against center line
translate([0, -od / 2, 0]) {
        closed_chamfered_tube(height, od, id, chamfer = chamfer, cutout = true);
}

rotate([0, 0, 180]) {
    rail(height = tallness, chamfer = chamfer, back_wall_gap = cap_back_wall_gap, copies = caps);
}


